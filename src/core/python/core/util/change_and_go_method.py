from collections import defaultdict
from typing import List, Tuple, Mapping, Sequence

from core.exceptions.config_exceptions import ConfigInvalidValueException
from core.model.change_and_go import CGType, CGEdge, CGNode
import itertools
from core.model.graph import Graph
from core.model.impl.simple_dict_graph import SimpleDictGraph
from core.model.lines import Line
from core.model.ptn import Link, Stop
import logging

logger = logging.getLogger(__name__)



def buildCGGraph(lines: List[Line], model_drive: str, model_wait: str,
                 model_change: str, ean_change_penalty: int, min_change_time: float, period_length: int,
                 min_wait_time: int, max_wait_time: int, directed: bool, use_frequencies: bool = False,
                 create_transfers: bool = True) -> Graph[CGNode, CGEdge]:
    """
    Builds an Change&Go graph, given a line pool
    and the drive/wait/transfer models. The length of the edges will depend on the models used. All drive edges
    will have drive+wait length, the transfer edges have change_time-wait length and the od edges have
    min_change_time+ean_change_penalty-wait_time length. This results in a shortest path from od node to od node being
    (2*min_changetime+2*ean_change_penalty-wait_time) longer than the travel time of the passenger. This needs to
    be respected when computing shortest paths
    :param lines: line pool to be transformed into a C&G graph
    :param model_drive: string specifying weight information used for line edges
    :param model_wait: string specifying weight information used for wait edges
    :param model_change: string specifying weight information used for transfer edges
    :param ean_change_penalty: penalty parameter for a transfer
    :param min_change_time: the minimal change time possible
    :param period_length: the period length used for the periodic timetable later
    :param min_wait_time: the minimal wait time possible
    :param max_wait_time: the maximal wait time possible
    :param directed: whether the lines are supposed to be directed
    :param use_frequencies: whether to use the frequencies of the lines to determine the transfer time between them.
    Otherwise ean_change_penalty+min_change_time will be used
    :param create_transfers: whether to create transfer edges
    :return: a Change&Go Graph
    """

    wait_time = compute_wait_time(model_wait, min_wait_time, max_wait_time)

    od_edge_weight = ean_change_penalty + period_length - wait_time if use_frequencies else ean_change_penalty + min_change_time - wait_time

    if od_edge_weight < 0:
        logger.warning("Chosen od weight is negative! This will result "
                       "in negative edge lengths in the change and go network and may lead to failing algorithms!")

    # determine weight function for line edges
    weight_function_drive = determine_weight_function_drive(model_drive)

    # determine weight function for transfer edges
    def weight_function_change(frequency_1: int, frequency_2: int) -> float:
        if not use_frequencies or model_change.upper() == "MINIMAL_CHANGING_TIME":
            return min_change_time + ean_change_penalty
        elif model_change.upper() == "FORMULA_1":
            return period_length / frequency_1 + period_length / frequency_2 + ean_change_penalty
        elif model_change.upper() == "FORMULA_2":
            return period_length / (2 * frequency_1 * frequency_2) + ean_change_penalty
        elif model_change.upper() == "FORMULA_3":
            return period_length / (2 * frequency_2) + ean_change_penalty
        else:
            raise ConfigInvalidValueException(["ean_model_weight_change"])

    cgn = SimpleDictGraph()
    node_id = 1
    edge_id = 1
    line_nodes_for_stops = {}
    od_nodes_for_stops = {}

    # generate line nodes, line edges, od nodes and od edges
    line_frequencies = {line.getId(): line.getFrequency() for line in lines}
    for line in lines:
        line_path = line.getLinePath()
        nodes = line_path.getNodes()
        edges = line_path.getEdges()
        prev_node = None

        for i in range(len(nodes)):
            node = nodes[i]
            if i != 0:
                edge = edges[i-1]
            else:
                edge = None

            # generate OD node if necessary
            if node.getId() not in list(od_nodes_for_stops.keys()):
                odnode = CGNode(node_id, node.getId(), 0)
                cgn.addNode(odnode)
                od_nodes_for_stops[node.getId()] = odnode
                node_id += 1

            # generate line node
            linenode = CGNode(node_id, node.getId(), line.getId())
            cgn.addNode(linenode)
            if not line_nodes_for_stops.get(node.getId()):
                line_nodes_for_stops[node.getId()] = []
            line_nodes_for_stops.get(node.getId()).append(linenode)
            node_id += 1

            # generate line edge if possible
            if edge:
                if prev_node:
                    cgn.addEdge(CGEdge(edge_id,
                                       prev_node,
                                       linenode,
                                       weight_function_drive(edge) + wait_time,
                                       CGType("\"line\""),
                                       directed=True))
                    edge_id += 1
                    if not directed:
                        cgn.addEdge(CGEdge(edge_id,
                                           linenode,
                                           prev_node,
                                           weight_function_drive(edge) + wait_time,
                                           CGType("\"line\""),
                                           directed=True))
                        edge_id += 1

            prev_node = linenode

            # generate od edge
            cgn.addEdge(CGEdge(edge_id,
                               od_nodes_for_stops.get(node.getId()),
                               linenode,
                               od_edge_weight,
                               CGType("\"OD\""),
                               directed=True))
            edge_id += 1
            cgn.addEdge(CGEdge(edge_id,
                               linenode,
                               od_nodes_for_stops.get(node.getId()),
                               od_edge_weight,
                               CGType("\"OD\""),
                               directed=True))
            edge_id += 1

    # generate C&G transfer edges
    if create_transfers:
        for stop in list(line_nodes_for_stops.keys()):
            linenodes_this_stop = line_nodes_for_stops.get(stop)
            all_stop_pairs = list(itertools.combinations(linenodes_this_stop, 2))

            for pair in all_stop_pairs:
                frequency_1 = line_frequencies[pair[0].getLineId()]
                frequency_2 = line_frequencies[pair[1].getLineId()]
                cgn.addEdge(CGEdge(edge_id,
                                   pair[0],
                                   pair[1],
                                   weight_function_change(frequency_1, frequency_2) - wait_time,
                                   CGType("\"transfer\""),
                                   directed=True))
                edge_id += 1
                cgn.addEdge(CGEdge(edge_id,
                                   pair[1],
                                   pair[0],
                                   weight_function_change(frequency_2, frequency_1) - wait_time,
                                   CGType("\"transfer\""),
                                   directed=True))
                edge_id += 1

    return cgn


def build_compact_cg_graph(
        stops: List[Stop], lines: Sequence[Line], model_drive: str,
        model_wait: str, min_waiting_time: int, max_waiting_time: int,
        directed: bool, create_transfers: bool = True,
        use_frequencies: bool = False,
        model_change: str = "MINIMAL_CHANGING_TIME",
        ean_change_penalty: int = 0, min_transfer_time: float = 5,
        period_length: int = 60) \
        -> Tuple[Graph[CGNode, CGEdge], Mapping[Line, Sequence[CGEdge]]]:

    waiting_time = compute_wait_time(model_wait, min_waiting_time,
                                     max_waiting_time)

    # determine weight function for line edges
    weight_function_drive = determine_weight_function_drive(model_drive)

    # determine weight function for transfer edges
    def weight_function_change(frequency: int) -> float:
        if (not use_frequencies
                or model_change.upper() == "MINIMAL_CHANGING_TIME"):
            return (min_transfer_time + ean_change_penalty) / 2
        elif model_change.upper() == "FORMULA_1":
            return period_length / frequency + ean_change_penalty / 2
        else:
            raise ConfigInvalidValueException(["ean_model_weight_change",
                                               "lc_model"])

    cgn = SimpleDictGraph[CGNode, CGEdge]()
    node_number = 1
    arc_number = 1
    line_arcs : Mapping[Line, list[CGEdge]] = defaultdict(list)
    # edge_arcs : Mapping[Link, list[CGEdge]] = defaultdict(list)
    central_nodes_for_stops = {}

    # generate central stop nodes
    for stop in stops:
        central_node = CGNode(node_number, stop.stop_id, 0)
        cgn.addNode(central_node)
        central_nodes_for_stops[stop.stop_id] = central_node
        node_number += 1

    # generate line nodes, line edges, and stop edges
    for line in lines:
        line_path = line.getLinePath()
        stops = line_path.getNodes()
        edges = line_path.getEdges()
        prev_node = None

        for i in range(len(stops)):
            stop = stops[i]

            # generate line node
            line_node = CGNode(node_number, stop.stop_id, line.line_id)
            cgn.addNode(line_node)
            node_number += 1

            # generate line arcs if possible
            if i > 0:
                edge = edges[i - 1]
                arc = CGEdge(arc_number, prev_node, line_node,
                             weight_function_drive(edge) + waiting_time,
                             CGType("\"line\""), directed=True)
                cgn.addEdge(arc)
                line_arcs[line].append(arc)
                # edge_arcs[edge].append(arc)
                arc_number += 1

                if not directed:
                    arc = CGEdge(arc_number, line_node, prev_node,
                                 weight_function_drive(edge) + waiting_time,
                                 CGType("\"line\""), directed=True)
                    cgn.addEdge(arc)
                    line_arcs[line].append(arc)
                    arc_number += 1

            prev_node = line_node

            # generate transfer arcs
            cgn.addEdge(CGEdge(
                arc_number, central_nodes_for_stops[stop.stop_id], line_node,
                weight_function_change(line.frequency) - waiting_time / 2,
                CGType("\"transfer\""), directed=True))
            arc_number += 1
            cgn.addEdge(CGEdge(
                arc_number, line_node, central_nodes_for_stops[stop.stop_id],
                weight_function_change(line.frequency) - waiting_time / 2,
                CGType("\"transfer\""), directed=True))
            arc_number += 1

    return cgn, line_arcs


def compute_wait_time(model: str, min_wait_time: int, max_wait_time: int):
    if model.upper() == "ZERO_COST":
        wait_time = 0
    elif model.upper() == "MINIMAL_WAITING_TIME":
        wait_time = min_wait_time
    elif model.upper() == "MAXIMAL_WAITING_TIME":
        wait_time = max_wait_time
    elif model.upper() == "AVERAGE_WAITING_TIME":
        wait_time = (min_wait_time + max_wait_time) / 2
    else:
        raise ConfigInvalidValueException(["ean_model_weight_wait"])
    return wait_time


def determine_weight_function_drive(model: str):
    def average_driving_time(link: Link):
        return (link.getUpperBound() + link.getLowerBound()) / 2
    if model.upper() == "AVERAGE_DRIVING_TIME":
        return average_driving_time
    elif model.upper() == "MINIMAL_DRIVING_TIME":
        return Link.getLowerBound
    elif model.upper() == "MAXIMAL_DRIVING_TIME":
        return Link.getUpperBound
    elif model.upper() == "EDGE_LENGTH":
        return Link.getLength
    else:
        raise ConfigInvalidValueException(["ean_model_weight_drive"])


def makeCGGraphDirected(CGGraph: Graph[CGNode, CGEdge]):
    """
    Transforms a given undirected Change&Go graph into a directed Change&Go graph.
    :param CGGraph: The undirected Change&Go graph to be transformed
    """
    CGDiGraph = CGGraph
    edges = CGDiGraph.getEdges()
    edge_id = len(edges) + 1
    for edge in edges:
        if edge.isDirected is not True:
            edge.setDirected(True)
            CGDiGraph.addEdge(
                CGEdge(edge_id, edge.getRightNode(), edge.getLeftNode(), edge.getWeight(), edge.getType(),
                       directed=True))
            edge_id += 1
        else:
            logger.warning(f"The given Change&Go Graph is not undirected!"
                           f"\nThe edge {str(edge)} is already directed.")
            return None
