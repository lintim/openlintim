import logging
import math
from typing import Union

import pulp
from pulp import LpVariable, LpAffineExpression, LpConstraint, constants, LpProblem, LpSolver, CPLEX_CMD, XPRESS, SCIP, \
    GLPK, COIN, PULP_CBC_CMD

from core.exceptions.exceptions import LinTimException
from core.exceptions.solver_exceptions import SolverAttributeNotImplementedException, SolverNotImplementedException, \
    SolverParamNotImplementedException
from core.solver.generic_solver_interface import Variable, VariableType, LinearExpression, Constraint, Model, \
    ConstraintSense, DoubleParam, IntParam, DoubleAttribute, Status, IntAttribute, OptimizationSense, SolverType, Solver


logger = logging.getLogger(__name__)


class PulpVariable(Variable):

    def __init__(self, var: LpVariable):
        self._var = var

    def getName(self) -> str:
        return self._var.getName()

    def getType(self) -> VariableType:
        if self._var.isFree():
            return VariableType.CONTINOUS
        elif self._var.isBinary():
            return VariableType.BINARY
        elif self._var.isInteger():
            return VariableType.INTEGER
        else:
            raise LinTimException(f"Unknown variable type: {self._var.cat}")

    def getLowerBound(self) -> float:
        return self._var.getLb()

    def getUpperBound(self) -> float:
        upper_bound = self._var.getUb()
        if upper_bound is None:
            upper_bound = math.inf
        return upper_bound

    def __add__(self, other):
        other_expr = extract_pulp_expression(other)
        if not isinstance(other_expr, (LpVariable, LpAffineExpression)):
            print(f"Calling var add with {type(other_expr)}")
        return PulpLinearExpression(self._var + other_expr)

    def __sub__(self, other):
        other_expr = extract_pulp_expression(other)
        if not isinstance(other_expr, (LpVariable, LpAffineExpression)):
            print(f"Calling var sub with {type(other_expr)}")
        return PulpLinearExpression(self._var - other_expr)

    def __rsub__(self, other):
        other_expr = extract_pulp_expression(other)
        if not isinstance(other_expr, (LpVariable, LpAffineExpression)):
            print(f"Calling var rsub with {type(other_expr)}")
        return PulpLinearExpression(other_expr - self._var)

    def __mul__(self, other: float):
        if isinstance(other, (int, float)):
            return PulpLinearExpression(other * self._var)

    def __rmul__(self, other: float):
        if isinstance(other, (int, float)):
            return PulpLinearExpression(other * self._var)

    def __str__(self) -> str:
        return f"PuLp Varible: {self._var}"


class PulpLinearExpression(LinearExpression):

    def __init__(self, expr: LpAffineExpression):
        self._expr = expr

    def add(self, other: Union["LinearExpression", Variable]):
        other_expr = extract_pulp_expression(other)
        if not isinstance(other_expr, (LpVariable, LpAffineExpression)):
            print(f"Calling addInPlace with {type(other_expr)}")
        self._expr.addInPlace(other_expr)

    def multiAdd(self, multiple: float, other: Union["LinearExpression", Variable]):
        other_expr = extract_pulp_expression(other)
        if not isinstance(other_expr, (LpVariable, LpAffineExpression)):
            print(f"Calling addInPlace with {type(other_expr)}")
        self._expr.addInPlace(multiple * other_expr)

    def clear(self):
        self._expr = LpAffineExpression()

    def addConstant(self, value: float):
        self._expr.constant += value

    def __add__(self, other):
        other_expr = extract_pulp_expression(other)
        if not isinstance(other_expr, (LpVariable, LpAffineExpression)):
            print(f"Calling add with {type(other_expr)}")
        return PulpLinearExpression(self._expr + other_expr)

    def __sub__(self, other):
        other_expr = extract_pulp_expression(other)
        if not isinstance(other_expr, (LpVariable, LpAffineExpression)):
            print(f"Calling sub with {type(other_expr)}")
        return PulpLinearExpression(self._expr - other_expr)

    def __rsub__(self, other):
        other_expr = extract_pulp_expression(other)
        if not isinstance(other_expr, (LpVariable, LpAffineExpression)):
            print(f"Calling rsub with {type(other_expr)}")
        return PulpLinearExpression(other_expr - self._expr)

    def __mul__(self, other: float):
        if isinstance(other, (int, float)):
            return PulpLinearExpression(other * self._expr)

    def __rmul__(self, other: float):
        if isinstance(other, (int, float)):
            return PulpLinearExpression(other * self._expr)

    def __str__(self) -> str:
        return f"PuLp Expression: {self._expr}"

class PulpConstraint(Constraint):

    def __init__(self, constr: LpConstraint):
        self._constr = constr

    def getName(self) -> str:
        return self._constr.getName()

    def getSense(self) -> ConstraintSense:
        if self._constr.sense == constants.LpConstraintEQ:
            return ConstraintSense.EQUAL
        if self._constr.sense == constants.LpConstraintGE:
            return ConstraintSense.GREATER_EQUAL
        if self._constr.sense == constants.LpConstraintLE:
            return ConstraintSense.LESS_EQUAL
        raise LinTimException(f"Unknown constraint sense {self._constr.sense}")

    def getRhs(self) -> float:
        return self._constr.constant

    def setRhs(self, value: float) -> None:
        self._constr.constant = value


    def __str__(self) -> str:
        return f"PuLp Constraint: {self._constr}"


def raise_for_non_pulp_variable(var: Variable):
    if not isinstance(var, PulpVariable):
        raise LinTimException("Try to work with non pulp variable in pulp context")


def raise_for_non_pulp_expression(expr: LinearExpression):
    if not isinstance(expr, PulpLinearExpression):
        raise LinTimException("Try to work with non pulp expression in pulp context")

def extract_pulp_expression(expression: Union[Variable, LinearExpression, int, float]) -> Union[LpAffineExpression, LpVariable, int, float]:
    if isinstance(expression, PulpLinearExpression):
        return expression._expr
    if isinstance(expression, PulpVariable):
        return expression._var
    if isinstance(expression, (int, float)):
        return expression
    raise LinTimException("Try to work with non pulp objects in pulp context")


class PulpModel(Model):

    def __init__(self, solver_type: SolverType):
        self._model = LpProblem()
        self._objective = LpAffineExpression()
        self._sense = OptimizationSense.MINIMIZE
        if not solver_type:
            raise LinTimException("Did not get a solver type for PulpModel, this is necessary to continue")
        self._solver_type = solver_type
        self._parameters = {}
        self._warm_start = False

    def dispose(self) -> None:
        del self._model
        del self._objective

    def getOriginalModel(self):
        return self._model

    def addVariable(self, lower_bound: float = 0, upper_bound: float = math.inf,
                    var_type: VariableType = VariableType.CONTINOUS, objective: float = 0, name: str = "") -> Variable:
        if var_type == VariableType.CONTINOUS:
            var_type = constants.LpContinuous
        elif var_type == VariableType.INTEGER:
            var_type = constants.LpInteger
        elif var_type == VariableType.BINARY:
            var_type = constants.LpBinary
        else:
            raise LinTimException(f"Unknown var type {var_type}")
        if upper_bound == math.inf:
            upper_bound = None
        new_var = LpVariable(name, lower_bound, upper_bound, var_type)
        if objective != 0:
            self._objective.addterm(new_var, objective)
        return PulpVariable(new_var)


    def addConstraint(self, lhs: Union[LinearExpression, Variable, float], sense: ConstraintSense,
                      rhs: Union[LinearExpression, Variable, float], name: str) -> Constraint:
        if sense == ConstraintSense.EQUAL:
            sense = constants.LpConstraintEQ
        elif sense == ConstraintSense.LESS_EQUAL:
            sense = constants.LpConstraintLE
        elif sense == ConstraintSense.GREATER_EQUAL:
            sense = constants.LpConstraintGE
        else:
            raise LinTimException(f"Unknown constraint sense {sense}")
        lhs_expr = extract_pulp_expression(lhs)
        rhs_expr = extract_pulp_expression(rhs)
        constr = LpConstraint(lhs_expr - rhs_expr, sense, name)
        self._model.addConstraint(constr)
        return PulpConstraint(constr)

    def getVariableByName(self, name: str) -> Variable:
        return PulpVariable(self._model.variablesDict()[name])

    def setStartValue(self, variable: PulpVariable, value: float):
        raise_for_non_pulp_variable(variable)
        variable._var.setInitialValue(value)
        self._warm_start = True

    def setObjective(self, objective: PulpLinearExpression, sense: OptimizationSense):
        raise_for_non_pulp_expression(objective)
        self._objective = objective._expr
        self.setSense(sense)

    def getObjective(self) -> LinearExpression:
        return PulpLinearExpression(self._objective)

    def createExpression(self) -> LinearExpression:
        return PulpLinearExpression(LpAffineExpression())

    def getSense(self) -> OptimizationSense:
        sense = self._model.getSense()
        if sense == constants.LpMinimize:
            return OptimizationSense.MINIMIZE
        if sense == constants.LpMaximize:
            return OptimizationSense.MAXIMIZE
        raise LinTimException(f"Unknown optimization sense {sense}")

    def write(self, filename: str):
        self._model.writeLP(filename)

    def setSense(self, sense: OptimizationSense):
        if sense == OptimizationSense.MAXIMIZE:
            sense = constants.LpMaximize
        elif sense == OptimizationSense.MINIMIZE:
            sense = constants.LpMinimize
        else:
            raise LinTimException(f"Unknown optimization sense {sense}")
        self._model.sense = sense

    def getStatus(self) -> Status:
        status = self._model.sol_status
        if status == constants.LpSolutionNoSolutionFound or status == constants.LpSolutionInfeasible:
            return Status.INFEASIBLE
        if status == constants.LpSolutionIntegerFeasible:
            return Status.FEASIBLE
        if status == constants.LpSolutionOptimal:
            return Status.OPTIMAL
        raise LinTimException(f"Unknown pulp solution status {status}")

    def solve(self):
        self._model.setObjective(self._objective)
        solver = self._initialize_solver()
        self._model.solve(solver)
        del solver

    def _initialize_solver(self) -> LpSolver:
        # Initialize the four possible parameters
        threads = None
        timelimit = None
        msg = False
        mip_gap = None
        for name, value in self._parameters.items():
            if name == IntParam.THREADS:
                if value > 0:
                    threads = value
            elif name == IntParam.OUTPUT_LEVEL:
                if value == logging.DEBUG:
                    msg = True
            elif name == IntParam.TIMELIMIT:
                if value > 0:
                    timelimit = value
            elif name == DoubleParam.MIP_GAP:
                if value > 0:
                    mip_gap = value
            else:
                raise SolverParamNotImplementedException(self._solver_type, name)
        if self._solver_type == SolverType.CPLEX:
            return CPLEX_CMD(timeLimit=timelimit, msg=msg, threads=threads, gapRel=mip_gap, warmStart=self._warm_start)
        elif self._solver_type == SolverType.XPRESS:
            options = []
            if threads:
                options.append(f"THREADS={threads}")
            return XPRESS(msg=msg, timeLimit=timelimit, gapRel=mip_gap, options=options)
        elif self._solver_type == SolverType.SCIP:
            if threads:
                logger.warning("Setting a thread limit is not supported by the PuLP Scip interface, skip")
            if mip_gap:
                logger.warning("Setting a mip gap is not supportted by the PuLP Scip interface, skip")
            return SCIP(msg=msg, timeLimit=timelimit)
        elif self._solver_type == SolverType.GLPK:
            if threads:
                logger.warning("Setting a thread limit is not supported by the PuLP GLPK interface, skip")
            if mip_gap:
                logger.warning("Setting a mip gap is not supportted by the PuLP GLPK interface, skip")
            return GLPK(msg=msg, timeLimit=timelimit)
        elif self._solver_type == SolverType.COIN:
            return COIN(msg=msg, timeLimit=timelimit, gapRel=mip_gap, warmStart=self._warm_start, threads=threads)
        elif self._solver_type == SolverType.CBC:
            return PULP_CBC_CMD(msg=msg, timeLimit=timelimit, maxSeconds=timelimit, gapRel=mip_gap,
                                warmStart=self._warm_start)
        else:
            raise SolverNotImplementedException(self._solver_type)

    def computeIIS(self, filename: str):
        raise NotImplementedError("Computing an IIS is not implemented for Pulp solver interface")

    def getValue(self, variable: PulpVariable):
        raise_for_non_pulp_variable(variable)
        return variable._var.valueOrDefault()

    def getIntAttribute(self, attribute: IntAttribute) -> int:
        if attribute == IntAttribute.NUM_VARIABLES:
            return self._model.numVariables()
        if attribute == IntAttribute.NUM_INT_VARIABLES:
            return len([var for var in self._model.variables() if var.isInteger()])
        if attribute == IntAttribute.NUM_BIN_VARIABLES:
            return len([var for var in self._model.variables() if var.isBinary()])
        if attribute == IntAttribute.NUM_CONSTRAINTS:
            return self._model.numConstraints()
        if attribute == IntAttribute.NUM_SOLUTIONS:
            logger.warning("IntAttribute.NUM_SOLUTIONS is not implemented for PuLP interface, determine based on"
                           "solution status")
            if self.getStatus() == Status.FEASIBLE or self.getStatus() == Status.OPTIMAL:
                return 1
            else:
                return 0
        raise SolverAttributeNotImplementedException(self._solver_type, attribute.name)

    def getDoubleAttribute(self, attribute: DoubleAttribute) -> float:
        if attribute == DoubleAttribute.OBJ_VAL:
            return pulp.value(self._model.objective)
        if attribute == DoubleAttribute.RUNTIME:
            return self._model.solutionTime
        if attribute == DoubleAttribute.MIP_GAP:
            logger.warning("DoubleAttribute MIP_GAP is not implemented for PuLP interface, give error value")
            return -1
        raise SolverAttributeNotImplementedException(self._solver_type, attribute.name)

    def setIntParam(self, param: IntParam, value: int):
        self._parameters[param] = value

    def setDoubleParam(self, param: DoubleParam, value: float):
        self._parameters[param] = value


class PulpSolver(Solver):

    def __init__(self, solver_type: SolverType = None):
        self.solver_type = solver_type

    def createModel(self) -> Model:
        return PulpModel(self.solver_type)

    def dispose(self) -> None:
        # Nothing to do here
        pass