from core.exceptions.exceptions import LinTimException


class SolverAttributeNotImplementedException(LinTimException):

    def __init__(self, type: "SolverType", attribute_name: str):
        super().__init__(f"Error S5: Attribute {attribute_name} is not implemented for {type.name} yet")


class SolverParamNotImplementedException(LinTimException):

    def __init__(self, type: "SolverType", param_name: str):
        super().__init__(f"Error S6: Parameter {param_name} is not implemented for {type.name} yet")


class SolverNotImplementedException(LinTimException):

    def __init__(self, sol_type: "SolverType"):
        super().__init__(f"Error S4: Solver {sol_type} is not implemented in python core")


class SolverFoundNoFeasibleSolutionException(LinTimException):

    def __init__(self):
        super().__init__(f"Error S10: Solver found no feasible solution. Check solver output for further information")