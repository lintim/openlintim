from core.exceptions.exceptions import LinTimException


class DataIndexNotFoundException(LinTimException):
    """
    Exception to throw if an element with a specific index is not found.
    """

    def __init__(self, element: str, index: int):
        """
        Exception to throw if an element with a specific index is not found.
        :param element: type of element which is searched
        :param index: index of the element
        """
        super().__init__("Error D3: {} with index {} not found.".format(element, index))


class DataIllegalEventTypeException(LinTimException):
    """
    Exception to throw if the type of an event is undefined.
    """

    def __init__(self, event_id: int, event_type: str):
        """
        Exception to throw if the type of an event is undefined.
        :param event_id: event id
        :param event_type: event type
        """
        super().__init__("Error D4: {} of event {} is no legal event type.".format(event_id, event_type))


class DataIllegalActivityTypeException(LinTimException):
    """
    Exception to throw if the type of an activity is undefined.
    """

    def __init__(self, activity_id: int, activity_type: str):
        """
        Exception to throw if the type of an activity is undefined.
        :param activity_id: activity id
        :param activity_type: activity type
        """
        super().__init__("Error D5: {} of activity {} is no legal activity type.".format(activity_type, activity_id))


class DataIllegalLineDirectionException(LinTimException):
    """
    Exception to throw if the direction of an event is undefined.
    """

    def __init__(self, event_id: int, value: str):
        """
        Exception to throw if the direction of an event is undefined.
        :param event_id: event id
        :param value: the false direction value
        """
        super().__init__("Error D6: {} of event {} is no legal line direction".format(value, event_id))


class DataLinePoolCostInconsistencyException(LinTimException):
    """
    Exception to throw when the number of read line costs does not match the number of lines in the corresponding pool.
    """

    def __init__(self, lines: int, read_line_costs: int, cost_file_name: str):
        """
        Exception to throw when the number of read line costs does not match the number of lines in the corresponding
        pool.
        :param lines: the number of lines in the line pool
        :param read_line_costs: the number of read line costs
        :param cost_file_name: the read cost file
        """
        super().__init__("Error D7: Read {} entries in the line cost file {}, but {} lines are in the line pool!"
                         .format(read_line_costs, cost_file_name, lines))


class DataRoutingMultiplePathsException(LinTimException):
    """
    Exception to throw when there are two paths for the same OD-pair in a routing file.
    """

    def __init__(self, node1_id: int, node2_id: int):
        """
        Exception to throw when there are two paths for the same OD-pair in a routing file.
        :param node1_id: id of the first node
        :param node2_id: id of the second node
        """
        super().__init__("Error D8: There are multiple paths given from {} to {}!"
                         .format(node1_id, node2_id))


class DataRoutingPathInconsistencyException(LinTimException):
    """
    Exception to throw when there is some inconsistency with a given path in a routing file.
    """

    def __init__(self, node1_id: int, node2_id: int):
        """
        Exception to throw when there is some inconsistency with a given path in a routing file.
        :param node1_id: id of the first node
        :param node2_id: id of the second node
        """
        super().__init__("Error D9: The path from {} to {} is not valid!"
                         .format(node1_id, node2_id))


class DataRoutingIncompleteException(LinTimException):
    """
    Exception to throw if the routing is incomplete but a complete routing is necessary.
    """

    def __init__(self):
        """
        Exception to throw if the routing is incomplete but a complete routing is necessary.
        """
        super().__init__("Error D10: The given routing is incomplete, but a complete routing is needed!")


class DataStopInNoZoneException(LinTimException):
    """
    Exception to throw if there is a node not assigned to any zone.
    """

    def __init__(self, stop_id: int):
        """
        Exception to throw if there is a node not assigned to any zone.
        """
        super().__init__("Error D11: The stop {} is not assigned to any zone!".format(stop_id))


class DataStopInMultipleZonesException(LinTimException):
    """
    Exception to throw if a node is assigned to multiple zones.
    """

    def __init__(self, stop_id: int):
        """
        Exception to throw if a node is assigned to multiple zones.
        """
        super().__init__("Error D12: The stop {} is assigned to more than one zone!".format(stop_id))


class DataPriceMatrixNotCompleteException(LinTimException):
    """
    Exception to throw if a price matrix does not contain a price for each non diagonal element.
    """

    def __init__(self, origin_id: int, destination_id: int):
        """
        Exception to throw if a price matrix does not contain a price for each non diagonal element.
        """
        super().__init__("Error D13: There is no price specified from {} to {}".format(origin_id, destination_id))


class DataZonePriceListInconsistencyException(LinTimException):
    """
    Exception to throw if in a zone price file not all prices from 1...n are specified.
    """

    def __init__(self):
        """
        Exception to throw if in a zone price file not all prices from 1...n are specified.
        """
        super().__init__("Error D14: Zone price file is inconsistent.")


class DataRidepoolAreaNotConnectedException(LinTimException):
    """
    Exception to throw if the edges of a ridepool area do not form a strongly connected graph.
    """

    def __init__(self, area_id: int):
        """
        Exception to throw if the edges of a ridepool area do not form a strongly connected graph.
        """
        super().__init__("Error D15: Ridepool area with id {} is not strongly connected!".format(area_id))


class DataRidepoolAreaInconsistencyException(LinTimException):
    """
    Exception to throw if there is an inconsistency during the creation of a Ridepool Area.
    """

    def __init__(self, area_id: int):
        """
        Exception to throw if there is an inconsistency during the creation of a Ridepool Area.
        """
        super().__init__("Error D16: Ridepool area with id {} can not be created!".format(area_id))
