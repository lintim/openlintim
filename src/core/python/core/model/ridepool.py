import logging
import bisect, math
from typing import Dict, List
import networkx as nx

from core.model.ptn import Link

from core.exceptions.data_exceptions import DataRidepoolAreaInconsistencyException

class RidepoolArea:
    """
    A class for representing a ridepool area.
    """
    logger = logging.getLogger(__name__)

    def __init__(self, area_id: int, period_length: int, vehicle_capacity: int, nb_vehicles: int = 0,
                 edges: List[Link]=[], compute_alpha: bool=True, alpha: Dict[Link, float] = {}):
        """
        Constructor for a ridepool area with given information.
        :param area_id: the id of the area
        :param period_length: period length to consider
        :param vehicle_capacity: capacit of a single vehicle in this area
        :param nb_vehicles: number of vehicles operating in the area
        :param edges: list of edges contained in the area
        :param compute_alpha: whether to compute the values of the demand factors alpha or just to use a given value
        :param alpha: dictionary specifying for each link in the area a demand factor alpha. Only used, if compute_alpha is False.
        """
        self.area_id = area_id
        self.nb_vehicles = nb_vehicles
        self.edges = []
        self.alpha = {}
        self.sum_of_weighted_durations = 0
        self.period_length = period_length
        self.capacity = vehicle_capacity
        if edges != list(alpha.keys()) and not compute_alpha:
            self.logger.error("Given alpha dictionary does not fit with the given edge list!")
            raise DataRidepoolAreaInconsistencyException(area_id)
        if compute_alpha:
            alpha = {edge: 0 for edge in edges}
        for link in edges[:-1]:
            self.addLink(link, alpha=alpha[link], compute_alpha=False)
        if edges:
            self.addLink(edges[-1], alpha=alpha[edges[-1]], compute_alpha=compute_alpha)


    def addLink(self, link: Link, alpha: float = 0, compute_alpha: bool = True) -> bool:
        """
        Method to add a new link to the ridepool area
        :param link: the link to add
        :param alpha: demand factor of the link in this area. Only used, if compute_alpha is False!
        :param compute_alpha: whether to compute the value of alpha or use a given value. Default: True
        :return: whether the link could be added
        """
        if link in self.edges:
            self.logger.warning("Link {} is already contained in ridepool area {}!".format(link.getId(), self.getId()))
            return False
        else:
            self.sum_of_weighted_durations += link.getLowerBound() * 2 * (math.ceil(link.getLoad()/self.capacity) + 1)//2
            if compute_alpha:
                self.alpha[link] = 2 * (math.ceil(link.getLoad()/self.capacity) + 1)//2 * self.period_length/self.sum_of_weighted_durations
                for edge in self.edges:
                    self.alpha[edge] = 2 * (math.ceil(edge.getLoad()/self.capacity) + 1)//2 * self.period_length/self.sum_of_weighted_durations
            else:
                self.alpha[link] = alpha
            bisect.insort(self.edges, link, key=lambda e: e.getId())
            return True

    def getId(self) -> int:
        """
        Gets the id of the area.
        :return: area id
        """
        return self.area_id

    def getNumberOfVehicles(self) -> int:
        """
        Gets the number of vehicles operating in the area
        :param nb_vehicles: number of vehicles
        """
        return self.nb_vehicles

    def getEdges(self) -> List[Link]:
        """
        Gets the edges belonging to the area.
        :return: the edges of the area
        """
        return self.edges

    def getDemandFactor(self, edge: Link) -> float:
        """
        Gets the demand factor alpha of an edge.
        :return: the demand factor alpha
        """
        return self.alpha[edge]

    def setNumberOfVehicles(self, nb_vehicles: int) -> None:
        """
        Sets the number of vehicles operating in the area
        :param nb_vehicles: number of vehicles
        """
        self.nb_vehicles = nb_vehicles

    def setDemandFactor(self, edge: Link, factor: float) -> None:
        """
        Sets the demand factor alpha of an edge.
        :param edge: the edge to set the factor for
        :param factor: factor to set
        """
        self.alpha[edge] = factor

    def compareEdges(self, other) -> bool:
        """
        Method to compare the edge sets of areas.
        :param other: area to compare
        """
        if not isinstance(other, RidepoolArea):
            return False
        return set(self.getEdges()) == set(other.getEdges())

    def isConnected(self) -> bool:
        """
        Method to test if the area is strongly connected.
        :return: whether the area is strongly connected
        """
        if len(self.getEdges()) == 0:
            return True
        if self.getEdges()[0].isDirected():
            G = nx.DiGraph([(edge.getLeftNode().getId(), edge.getRightNode().getId()) for edge in self.getEdges()])
        else:
            G = nx.DiGraph([(edge.getLeftNode().getId(), edge.getRightNode().getId()) for edge in self.getEdges()]
                           +[(edge.getRightNode().getId(), edge.getLeftNode().getId()) for edge in self.getEdges()])
        return nx.is_strongly_connected(G)

    def includesEdge(self, link: Link) -> bool:
        """
        Method to test if a given edge is yet included in the area.
        :param link: link to test
        :return: whether the edge is included or not
        """
        return link in self.edges

    def __eq__(self, other):
        if not isinstance(other, RidepoolArea):
            return False
        return (self.getId() == other.getId()
                and math.isclose(self.getNumberOfVehicles(), other.getNumberOfVehicles())
                and math.isclose(self.getCost(), other.getCost())
                and self.getEdges() == other.getEdges())

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self) -> int:
        return self.area_id

    def __str__(self):
        return ("Area " + self.getId() + ", costs = " + self.getCost() + "\nEdge; demand factor\n"
                + "\n".join(["{}: {}".format(edge.getId(), self.alpha[edge])
                             for edge in self.getEdges()]))


class RidePool:
    """
    A class to represent the ridepool.
    """

    def __init__(self, cost: float = 0):
        """
        Constructor of a new empty ridepool.
        :param cost: costs of a single vehicle operating in one area of the ridepool
        """
        self.rpool = {}  # type: Dict[int, RidepoolArea]
        self.costs = cost

    def addArea(self, area: RidepoolArea) -> bool:
        """
        Method to add a ridepool area, if not already an area with the same id is in the
        pool.
        :param area: the ridepool area to add
        :return: whether the area could be added.
        """
        if area.getId() in self.rpool:
            return False
        for other in self.rpool.values():
            if set(area.getEdges()) == set(other.getEdges()) and area.alpha == other.alpha:
                return False
        self.rpool[area.getId()] = area
        return True

    def removeLine(self, area_id: int) -> bool:
        """
        Method to remove area with given id, if it exists in pool.
        :param area_id: id of the area to remove
        :return: whether an area was removed
        """
        if area_id not in self.rpool:
            return False
        del self.rpool[area_id]
        return True

    def getAreas(self) -> List[RidepoolArea]:
        """
        Gets a list of the ridepool areas. This is a copy, i.e., removing or adding
        areas to the list will not change the ridepool.
        :return: the areas in the pool
        """
        return list(self.rpool.values())

    def getArea(self, area_id: int) -> RidepoolArea:
        """
        Gets the ridepool area for a given id or raises KeyError if it is not in the
        pool.
        :param area_id: id of the area to get
        :return: the area with the given id
        """
        return self.rpool[area_id]

    def getRideConcept(self) -> List[RidepoolArea]:
        """
        Method to get a list of all ridepool areas with number of vehicles > 0.
        :return: a list of all ridepool areas with number of vehicles > 0
        """
        return [area for area in self.rpool.values() if area.getNumberOfVehicles() > 0]

    def getCost(self) -> float:
        """
        Gets the cost of one vehicle operating in an area in the ridepool
        :return: cost of one vehicle operating in the area
        """
        return self.costs

    def setCost(self, cost: float) -> None:
        """
        Sets the cost of one vehicle operating in an area in the ridepool
        :param cost: cost of one vehicle operating in the area
        """
        self.costs = cost

    def testConnected(self) -> int:
        """
        Method to test if all areas in the ridepool are strongly connected.
        :return: the id of the area which is not strongly connected, or -1 of all areas are strongly connected.
        """
        for area in self.rpool.values():
            if not area.isConnected():
                return area.getId()
        return -1

    def __eq__(self, other):
        if not isinstance(other, RidePool):
            return False
        return self.rpool == other.rpool

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return ("RideConcept:\n"
                + "\n".join(["{}: {}".format(area.getId(), area.getNumberOfVehicles())
                             for area in self.getAreas()]))
