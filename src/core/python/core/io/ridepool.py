import logging

from core.exceptions.data_exceptions import DataIndexNotFoundException, DataRidepoolAreaNotConnectedException
from core.exceptions.input_exceptions import InputFormatException, InputTypeInconsistencyException
from core.io.csv import CsvReader, CsvWriter
from core.model.graph import Graph
from core.model.ridepool import RidePool, RidepoolArea
from core.model.ptn import Link, Stop
from core.util.config import Config


class RidepoolReader:
    """
    Class to process csv-lines, formatted in the LinTim Ridepool.giv or Ride-Concept.lin format.
    Use a CsvReader with the appropriate processing methods as an argument to read the files.
    """
    logger = logging.getLogger(__name__)

    def __init__(self, ridepool_file_name: str, ride_pool: RidePool, ptn: Graph[Stop, Link], 
                 read_number_vehicles: bool, read_alpha: bool=False, period_length: int = 0, vehicle_capacity: int = 0):
        """
        Constructor of a RidepoolReader for a ride pool or ride concept (depending on read_number_vehicles) and a given
        filename. The given name will not influence the read file but the used name in any error message, so be sure to
        use the same name in here and in the CsvReader!
        :param ridepool_file_name: source file name for exceptions
        :param ride_pool: ride pool
        :param ptn: the base ptn
        :param read_number_vehicles: whether a ride pool or a ride concept with number of vehicles is read
        :param read_alpha: Whether to read the alpha values or to compute them
        :param period_length: The period length. Needed to compute the alpha values
        :param vehicle_capacity: Capacity of a single ridepooling vehicle. Needed to compute the alpha values
        """
        self.ridepool_file_name = ridepool_file_name
        self.ride_pool = ride_pool
        self.ptn = ptn
        self.read_number_vehicles = read_number_vehicles
        self.read_alpha = read_alpha
        self.period_length = period_length
        self.vehicle_capacity = vehicle_capacity

    def process_ride_pool_line(self, args: [str], line_number: int) -> None:
        """
        Process the contents of a ridepool or rideconcept line.
        :param args: the content of the line
        :param line_number: the line number, used for error handling
        :param period_length: period length to compute the alpha if not read froom the file
        """
        if not self.read_number_vehicles and len(args) != 3:
            raise InputFormatException(self.ridepool_file_name, len(args), 3)
        elif self.read_number_vehicles and len(args) != 4:
            raise InputFormatException(self.ridepool_file_name, len(args), 4)
        try:
            area_id = int(args[0])
        except ValueError:
            raise InputTypeInconsistencyException(self.ridepool_file_name, 1, line_number, "int", args[0])
        try:
            link_id = int(args[1])
        except ValueError:
            raise InputTypeInconsistencyException(self.ridepool_file_name, 2, line_number, "int", args[2])
        if self.read_alpha:
            try:
                alpha = float(args[2])
            except ValueError:
                raise InputTypeInconsistencyException(self.ridepool_file_name, 3, line_number, "float", args[1])
        else:
            alpha = 0
        if self.read_number_vehicles:
            try:
                nb_vehicles = int(args[3])
            except ValueError:
                raise InputTypeInconsistencyException(self.ridepool_file_name, 4, line_number, "int", args[3])
        else:
            nb_vehicles = 0
        try:
            area = self.ride_pool.getArea(area_id)
            created_new_area = False
        except KeyError:
            area = RidepoolArea(area_id, period_length=self.period_length, vehicle_capacity=self.vehicle_capacity, nb_vehicles=nb_vehicles)
            created_new_area = True
        link = self.ptn.getEdge(link_id)
        if not link:
            raise DataIndexNotFoundException("Link", link_id)
        area.addLink(link, alpha=alpha, compute_alpha=not self.read_alpha)

        if self.read_number_vehicles and created_new_area:
            try:
                number_of_vehicles = int(args[3])
            except ValueError:
                raise InputTypeInconsistencyException(self.ridepool_file_name, 4, line_number, "int", args[3])
            area.setNumberOfVehicles(number_of_vehicles)
        self.ride_pool.addArea(area)

    @staticmethod
    def read(ptn: Graph[Stop, Link], read_areas: bool = True, read_number_vehicles: bool = True,
             read_alpha: bool=False, ridepool_file_name: str = "", ride_pool: RidePool = None, period_length: int = 0,
             vehicle_capacity: int=0, config: Config = Config.getDefaultConfig()) -> RidePool:
        """
        Read the ride pool or the ride concept, depending on read_number_vehicles. Read areas will be added to
        the given RidePool, if there is one.
        :param ptn: the base PTN
        :param read_areas: Whether to read the areas
        :param read_number_vehicles: Whether to read the number of vehicles of each area, i.e. whether to read a ride pool or ride concept
        :param read_alpha: whether to read or to compute the values of alpha
        :param ridepool_file_name: the ride pool file name to read.
        :param ride_pool: a given ridepool to add the read areas to. If there is none, a new RidePool will be created.
        :param period_length: The period length. Needed to compute the alpha values
        :param vehicle_capacity: Capacity of a ridepooling vehicle. Needed to compute the alpha values 
        :param config: The config to use
        :return: the ride pool with the added areas.
        """
        if not read_areas and read_number_vehicles:
            RidepoolReader.logger.warning("Can not read number of vehicles but no areas, will read areas as well!")
            read_areas = True
        if not ride_pool:
            ride_pool = RidePool()
            ride_pool.setCost(config.getDoubleValue("rpool_costs_fixed"))
        if read_areas and not ridepool_file_name:
            if read_number_vehicles:
                ridepool_file_name = config.getStringValue("filename_rc_file")
            else:
                ridepool_file_name = config.getStringValue("filename_rpool_file")
        if not period_length:
            period_length = config.getIntegerValue("period_length")
        if not vehicle_capacity:
            vehicle_capacity = config.getIntegerValue("rc_passengers_per_vehicle")
        reader = RidepoolReader(ridepool_file_name, ride_pool, ptn, read_number_vehicles, read_alpha, period_length, vehicle_capacity)
        if read_areas:
            CsvReader.readCsv(ridepool_file_name, reader.process_ride_pool_line)
        area_id = ride_pool.testConnected()
        if area_id != -1:
            raise DataRidepoolAreaNotConnectedException(area_id)
        return ride_pool


class RidepoolWriter:
    """
    Class implementing writing ridepools and concepts as static methods. Use the static methods to write.
    """
    logger = logging.getLogger(__name__)

    @staticmethod
    def write(pool: RidePool, write_pool: bool = True, write_ride_concept: bool = True,
              pool_file_name: str = "", pool_header: str = "", 
              concept_file_name: str = "", concept_header: str = "", config: Config = Config.getDefaultConfig()):
        """
        Write the given ridepool, with or without the number of vehicles per area. write_pool and write_ride_concept 
        can be used to determine what to write. If no file name or header is given and the respective file should be 
        written, the values are read from the given config (or the default config, if there is none).
        :param pool: the ridepool to write
        :param write_pool: whether to write the given pool
        :param write_ride_concept: whether to write additionally the number of vehicles per area
        :param pool_file_name: the name of the file to write the pool to
        :param pool_header: the header to write in the pool file
        :param concept_file_name: the name of the file to write the ride concept to
        :param concept_header: the header to write in the ride concept file
        :param config: the config to use to read file names or headers, if necessary. Will use the default config if
        none is given
        """
        # Sort the areas first
        areas = pool.getAreas()
        areas.sort(key=RidepoolArea.getId)
        if write_pool:
            if not pool_file_name:
                pool_file_name = config.getStringValue("filename_rpool_file")
            if not pool_header:
                pool_header = config.getStringValue("rpool_header")
            pool_writer = CsvWriter(pool_file_name, pool_header)
            for area in areas:
                for link in area.getEdges():
                    pool_writer.writeLine([str(area.getId()), str(link.getId()), str(round(area.getDemandFactor(link), 3))])
            pool_writer.close()
        if write_ride_concept:
            if not concept_file_name:
                concept_file_name = config.getStringValue("filename_rc_file")
            if not concept_header:
                concept_header = config.getStringValue("rc_header")
            concept_writer = CsvWriter(concept_file_name, concept_header)
            for area in areas:
                for link in area.getEdges():
                    concept_writer.writeLine(
                        [str(area.getId()), str(link.getId()), str(round(area.getDemandFactor(link),3)), str(area.getNumberOfVehicles())])
            concept_writer.close()
