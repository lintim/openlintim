import logging
import numpy as np
import gurobipy as gp
from gurobipy import GRB
from typing import List, Tuple
from core.model.ptn import Stop
from InstanceClass import Instance


logger = logging.getLogger(__name__)

def p_best_centers(NodeList: List[Stop], interactions: List[int], p: float) -> List[Stop]:
    """ given a sorted list of nodes, p_best_centers returns the ceil(n*p)-first centers """
    logger.debug("Entered p_best_centers")
    mask = np.zeros((len(interactions)))
    mask[[int(node.getId()-1) for node in NodeList]] = 1
    mask = mask>0
    w_curr = interactions[mask]
    perm = np.argsort((-1)*w_curr)
    Nodes_sorted = []
    for j in perm:
        Nodes_sorted.append(NodeList[j])
    n = len(Nodes_sorted)
    numb_centers = int(np.ceil(n*p))
    Centers = Nodes_sorted[0:numb_centers]
    Centers = Centers + [node for node in NodeList if interactions[int(node.getId()-1)]==interactions[int(Nodes_sorted[int(numb_centers-1)].getId()-1)] and node not in Centers]
    logger.debug("Exiting p_best_centers")
    return Centers

def find_gaps(NodeList: List[Stop], instance: Instance) -> List[Stop]:
    logger.debug("Entered find_gaps")
    mask = np.zeros((len(instance.interactions)))
    mask[[int(node.getId()-1) for node in NodeList]] = 1
    mask = mask>0
    interactions_curr = np.where(mask, instance.interactions, np.ones((instance.interactions.shape[0])) * -np.inf)
    perm = np.argsort((-1)*interactions_curr)
    interactions_curr = interactions_curr[perm]
    interactions_curr = interactions_curr[interactions_curr>-np.inf]
    Nodes_sorted = []
    for j in perm:
        Nodes_sorted.append(instance.ptn.getNode(j+1))
    n = len(Nodes_sorted)
    numb_p_centers = int(np.ceil(n*instance.p_centers))
    diff = interactions_curr[:-1] - interactions_curr[1:]
    cut_idx = np.argmax(diff[max(0, int(np.floor(numb_p_centers - n*0.2))):min(len(diff), int(np.ceil(numb_p_centers + n*0.2)))]) + max(0,int(np.floor(numb_p_centers - n*0.2)))
    Centers_pot = Nodes_sorted[0:cut_idx+1]
    logger.debug("Exiting find_gaps")
    return Centers_pot

def preprocess_centers(instance: Instance) -> List[Stop]:
    """ preprocess_centers returns a list of possible centers (chosen by p_best_centers) and a list of endstations """
    logger.debug("Entered preprocess_centers")
    Endstations = [node for node in instance.Nodes if (instance.Degrees[node]==1)]
    Centers_curr = [node for node in instance.Nodes if (instance.Degrees[node]>=instance.minimal_node_degree_for_center)]
    Centers_pot = find_gaps(Centers_curr, instance)
    instance.Endstations = Endstations
    logger.debug("Exiting preprocess_centers")
    return Centers_pot

def circle_centers(Centers_pot: List[Stop], instance: Instance, cummuliert: bool= False) -> None:
    """ chooses centers from a list of potential centers """
    logger.debug("Entered circle_centers")
    weights = np.zeros((instance.numb_nodes,))                    # List: for each node the weight of the circle around it with radius r
    dist = np.zeros((instance.numb_nodes,instance.numb_nodes))    # Dictionary: For each node a list of the ids of the nodes, lying inside the circle with radius r around it
    dist[instance.distances<=instance.center_radius] = 1
    for curr_center in Centers_pot:
        weights[curr_center.getId()-1] = cummuliert * np.sum(instance.interactions[dist[curr_center.getId()-1,:]>0]) + (1-cummuliert) * instance.interactions[curr_center.getId()-1]

    try:
        with gp.Env("Circle_Choice", empty=True) as env:
            env.setParam('LogToConsole', 0)
            env.start()
            with gp.Model(env=env) as m:

                #m.setParam('LogToConsole',0)
                x = m.addMVar(shape = instance.numb_nodes, vtype = GRB.BINARY, name = "x")
                k = m.addMVar(shape = (instance.numb_nodes,instance.numb_nodes), vtype=GRB.BINARY, name = "k")
                m.setObjective(weights @ x, GRB.MAXIMIZE)
                for i in [node.getId()-1 for node in instance.Nodes if node not in Centers_pot]:
                    m.addConstr(x[i] == 0, name = "NotCenter"+str(i))
                for v in range(instance.numb_nodes):
                    m.addConstr(k[:,v].sum() <= 1, name = "Sum_k"+str(v))
                    for u in range(instance.numb_nodes):
                        m.addConstr(k[u,v] - dist[u,v]*x[u] == 0, name = "k=z"+str(v)+str(u))

                logger.debug("Start optimization")
                m.optimize()
                logger.debug("Finished optimization")

                optVal = m.objVal
                nodes_incidence = x.X

                logger.debug(f"nodes_incidence: {nodes_incidence}")
                for i in range(instance.numb_nodes):
                    if int(round(float(x[i].X))) == 1:
                        instance.Centers = instance.Centers + [instance.Nodes[i]]
                logger.debug("Exiting circle_centers")
                m.dispose()
                env.dispose()

    except gp.GurobiError as e:
        print('Error code ' + str(e.errno) + ": " + str(e))

    except AttributeError as e:
        print('Encountered an atribute error')
        print("Error message: " + str(e))


def sortOD(nodes: List[Stop], instance: Instance) -> List[Tuple[int, Tuple[Stop, Stop]]]:
    """ sorts pairs of nodes by their od-data """
    logger.debug("Entered sortOD")
    w = []
    for i in nodes:
        for j in nodes:
            if j.getId() < i.getId() or instance.directed:
                w.append((instance.od.getValue(i.getId(),j.getId()) + instance.od.getValue(j.getId(),i.getId()), (i, j)))
    def firstEntry(e):
        return e[0]
    w.sort(reverse = True, key = firstEntry)
    logger.debug("Exiting sortOD")
    return w

def choosingPeriphery(instance: Instance) -> None:
    """ returns a list of all nodes classified as Periphery """
    logger.debug("Entered choosingPeriphery")
    mean_dist = 0
    numb_centers = len(instance.Centers)
    for center in instance.Centers:
        mean_dist += np.sum(instance.distances[center.stop_id-1,:])/(instance.numb_nodes - 1)
    mean_dist = mean_dist/numb_centers

    instance.weighted_mean_dist = mean_dist*instance.p_periphery
    # instance.weighted_mean_dist = min(mean_dist*p_periphery, center_radius)                   # for experimental use

    instance.Periphery = list(set(instance.Endstations + [node for node in instance.Nodes if instance.distances[instance.closest_Centers[node][0].getId()-1,node.getId()-1] > instance.weighted_mean_dist]) - set(instance.Centers))
    def distnextcenter(node: Stop) -> float:
        return instance.distances[instance.closest_Centers[node][0].getId()-1,node.getId()-1]
    instance.Periphery.sort(reverse = True, key = distnextcenter)
    if not instance.Periphery:
        logger.warning("No peripheries chosen.")
    logger.debug("Exiting choosingPeriphery")
