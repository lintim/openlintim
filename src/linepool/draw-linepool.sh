#!/usr/bin/env bash

PROGRAMPATH=`dirname $0`

source ${PROGRAMPATH}/../base.sh

ant -q -f ${PROGRAMPATH}/build.xml build-linepool
java "${JFLAGS[@]}" -cp ${CLASSPATH}${PATHSEP}${CORE_DIR}/java/lintim-core.jar${PATHSEP}${PROGRAMPATH}/../essentials/lp-helper${PATHSEP}${PROGRAMPATH}/build DrawLinepool "$@"

if [[ $2 == true ]]; then
    DOTFILE=`"${CONFIGCMD[@]}" -s default_line_graph_file -u`
else
    DOTFILE=`"${CONFIGCMD[@]}" -s default_pool_graph_file -u`
fi
PNGFILE="${DOTFILE%.dot}.png"
PSFILE="${DOTFILE%.dot}.ps"
echo $2
neato -n -Tpng $DOTFILE -o $PNGFILE
neato -n -Tps $DOTFILE -o $PSFILE


EXITSTATUS=$?

exit ${EXITSTATUS}
