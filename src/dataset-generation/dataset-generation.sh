#!/usr/bin/env bash

PROGRAMPATH=`dirname $0`

source ${PROGRAMPATH}/../base.sh

DG_MODEL=`"${CONFIGCMD[@]}" -s dg_model -u`
PTN_NAME=`"${CONFIGCMD[@]}" -s ptn_name -u`
JGRAPHT_JAR_FILE=${PROGRAMPATH}/../../libs/jgrapht/jgrapht-core-1.1.0.jar
CORE_JAR=${CORE_DIR}/java/lintim-core.jar

function exit_on_build_failure {
    exit_code=$1
    if [ "$exit_code" -ne 0 ]; then
        echo "Failed to build project.";
        exit 1
    fi
}

cp -R ${PROGRAMPATH}/../../datasets/template ${PROGRAMPATH}/../../datasets/${PTN_NAME}
cp ${PROGRAMPATH}/../../datasets/dataset-generation/basis/Config.cnf ${PROGRAMPATH}/../../datasets/${PTN_NAME}/basis/Config.cnf

if [[ ${DG_MODEL,,} == "parametrized_city" ]]; then
	bash ${PROGRAMPATH}/parametrized-city/run.sh $1
elif [[ ${DG_MODEL,,} == "ring" ]]; then
	bash ${PROGRAMPATH}/ring/run.sh $1
else
	echo "Error: Requested DG_MODEL \"${DG_MODEL}\" not available!"
	exit 1
fi

EXITSTATUS=$?

exit ${EXITSTATUS}
