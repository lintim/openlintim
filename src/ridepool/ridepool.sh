#!/usr/bin/env bash

PROGRAMPATH=`dirname $0`

source ${PROGRAMPATH}/../base.sh

rpool_model=`"${CONFIGCMD[@]}" -s rpool_model -u`

if [[ ${rpool_model,,} == all ]]; then
	bash ${PROGRAMPATH}/all/run.sh ${1}
elif [[ ${rpool_model,,} == demand_heuristic ]]; then
	bash ${PROGRAMPATH}/demand_heuristic/run.sh ${1}
elif [[ ${rpool_model,,} == tree_based ]]; then
	bash ${PROGRAMPATH}/tree-based/run.sh ${1}
else
	echo "Error: Invalid rpool_model argument: ${rpool_model}"
	exit 1
fi

EXITSTATUS=$?

exit ${EXITSTATUS}