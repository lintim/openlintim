import logging
import sys

from core.exceptions.config_exceptions import ConfigNoFileNameGivenException
from core.io.config import ConfigReader
from core.io.ptn import PTNReader
from core.model.ridepool import RidePool, RidepoolArea
from core.io.ridepool import RidepoolWriter

logger = logging.getLogger(__name__)

if __name__ == '__main__':
    logger.info("Begin reading configuration")
    if len(sys.argv) < 2:
        raise ConfigNoFileNameGivenException()
    config = ConfigReader.read(sys.argv[1])

    cost = config.getDoubleValue("rpool_costs_fixed")
    period_length = config.getIntegerValue("period_length")
    vehicle_capacity = config.getIntegerValue("rc_passengers_per_vehicle")
    logger.info("Finished reading configuration")

    logger.info("Begin reading input data")
    ptn = PTNReader.read(read_loads=True)
    logger.info("Finished reading input data")

    logger.info("Begin computing ridepool")
    area = RidepoolArea(1, period_length, vehicle_capacity)
    for edge in ptn.getEdges():
        area.addLink(edge)
    rpool = RidePool()
    rpool.addArea(area)
    rpool.setCost(cost)
    logger.info("Finished computing ridepool")

    logger.info("Begin writing output data")
    RidepoolWriter.write(rpool, write_ride_concept=False)
    logger.info("Finished writing output data")