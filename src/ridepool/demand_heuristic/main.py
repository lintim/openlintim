import logging
import sys
from typing import List

from core.exceptions.config_exceptions import ConfigNoFileNameGivenException
from core.io.config import ConfigReader
from core.io.ptn import PTNReader
from core.io.lines import LineReader
from core.model.ridepool import RidePool, RidepoolArea
from core.model.ptn import Stop, Link
from core.model.graph import Graph
from core.io.ridepool import RidepoolWriter

logger = logging.getLogger(__name__)

def explore_edges(edges: List[Link], area: RidepoolArea, factor: float, train_capacity: int, ptn: Graph[Stop, Link], max_edges: int):
        edges_to_explore = set()
        for edge in edges:
            if len(area.getEdges()) >= max_edges:
                return area
            if edge.getLoad() <= factor*train_capacity and not area.includesEdge(edge):
                # print([e.getId() for e in area.getEdges()])
                # print(edge.getId())
                area.addLink(edge)
                for e in ptn.getIncidentEdges(edge.getLeftNode()) + ptn.getIncidentEdges(edge.getRightNode()):
                    if e not in area.getEdges():
                        edges_to_explore.add(e)
        if edges_to_explore != set():
            explore_edges(list(edges_to_explore), area, factor, train_capacity, ptn, max_edges)
        return area


if __name__ == '__main__':
    logger.info("Begin reading configuration")
    if len(sys.argv) < 2:
        raise ConfigNoFileNameGivenException()
    config = ConfigReader.read(sys.argv[1])

    cost = config.getDoubleValue("rpool_costs_fixed")
    period_length = config.getIntegerValue("period_length")
    min_edges = config.getIntegerValue("rpool_min_edges")
    max_edges = config.getIntegerValue("rpool_max_edges")
    vehicle_capacity = config.getIntegerValue("rc_passengers_per_vehicle")
    train_capacity = config.getIntegerValue("gen_passengers_per_vehicle")
    factor = config.getDoubleValue("rpool_load_factor")
    logger.info("Finished reading configuration")

    logger.info("Begin reading input data")
    ptn = PTNReader.read(read_loads=True)
    lpool = LineReader.read(ptn, read_frequencies=False)
    logger.info("Finished reading input data")


    logger.info("Begin computing ridepool")
    rpool = RidePool(cost)
    area_id = 1

    for edge in ptn.getEdges():
        if edge.getLoad() <= factor*train_capacity:
            area = RidepoolArea(area_id, period_length, vehicle_capacity, edges=[edge])
            area = explore_edges(ptn.getIncidentEdges(edge.getLeftNode()) + ptn.getIncidentEdges(edge.getRightNode()), area, factor, train_capacity, ptn, max_edges)
            if len(area.getEdges())>=min_edges and all(not area.compareEdges(other) for other in rpool.getAreas()):
                rpool.addArea(area)
                area_id += 1

    logger.info("Finished computing ridepool")


    logger.info("Begin writing output data")
    RidepoolWriter.write(rpool, write_ride_concept=False)
    logger.info("Finished writing output data")