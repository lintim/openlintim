import logging
import sys
from typing import List
import networkx as nx

from core.exceptions.config_exceptions import ConfigNoFileNameGivenException
from core.io.config import ConfigReader
from core.io.ptn import PTNReader
from core.io.lines import LineReader
from core.model.ridepool import RidePool, RidepoolArea
from core.model.ptn import Stop, Link
from core.model.graph import Graph
from core.io.ridepool import RidepoolWriter
from core.util.networkx import convert_graph_to_networkx

logger = logging.getLogger(__name__)

def explore_edges(edges: List[Link], area: RidepoolArea, factor: float, train_capacity: int, ptn: Graph[Stop, Link], max_edges: int):
        edges_to_explore = set()
        for edge in edges:
            if edge.getLoad() <= factor*train_capacity and len(area.getEdges())<max_edges and not area.includesEdge(edge):
                print([e.getId() for e in area.getEdges()])
                print(edge.getId())
                area.addLink(edge)
                for e in ptn.getIncidentEdges(edge.getLeftNode()) + ptn.getIncidentEdges(edge.getRightNode()):
                    if e not in area.getEdges():
                        edges_to_explore.add(e)
        if edges_to_explore != set():
            explore_edges(list(edges_to_explore), area, factor, train_capacity, ptn, max_edges)
        return area


if __name__ == '__main__':
    logger.info("Begin reading configuration")
    if len(sys.argv) < 2:
        raise ConfigNoFileNameGivenException()
    config = ConfigReader.read(sys.argv[1])

    cost = config.getDoubleValue("rpool_costs_fixed")
    period_length = config.getIntegerValue("period_length")
    min_edges = config.getIntegerValue("rpool_min_edges")
    max_edges = config.getIntegerValue("rpool_max_edges")
    vehicle_capacity = config.getIntegerValue("rc_passengers_per_vehicle")
    train_capacity = config.getIntegerValue("gen_passengers_per_vehicle")
    factor = config.getDoubleValue("rpool_load_factor")
    logger.info("Finished reading configuration")

    logger.info("Begin reading input data")
    ptn = PTNReader.read(read_loads=True)
    lpool = LineReader.read(ptn, read_frequencies=False)
    logger.info("Finished reading input data")


    logger.info("Begin computing ridepool")

    rpool = RidePool(cost)
    area_id = 1

    G = convert_graph_to_networkx(ptn, multi_graph=False, weight_function=lambda e: e.getLoad())
    T = nx.maximum_spanning_tree(G)

    for stop in ptn.getNodes():
        # iterate over all leafe nodes of max spanning tree
        if T.degree(stop.getId()) == 1:
            area = RidepoolArea(area_id, period_length, vehicle_capacity)
            added_nodes = []
            for edge in ptn.getIncidentEdges(stop):
                # add all incident edge, that are not in MST
                if not T.has_edge(edge.getLeftNode().getId(), edge.getRightNode().getId()):
                    area.addLink(edge)
                    if edge.getLeftNode().getId() == stop.getId():
                        added_nodes.append(edge.getRightNode())
                    else:
                        added_nodes.append(edge.getLeftNode())
            for stop1 in added_nodes:
                for stop2 in added_nodes:
                    # add all edges, that connect two newly added edges, but are not in MST
                    if G.has_edge(stop1.getId(), stop2.getId()) and not T.has_edge(stop1.getId(), stop2.getId()):
                        area.addLink(ptn.get_edge_by_nodes(stop1, stop2))
            if len(area.getEdges())>=min_edges and len(area.getEdges())<=max_edges:
                rpool.addArea(area)
                area_id += 1

    logger.info("Finished computing ridepool")


    logger.info("Begin writing output data")
    RidepoolWriter.write(rpool, write_ride_concept=False)
    logger.info("Finished writing output data")