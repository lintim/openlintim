import logging

import networkx as nx
from networkx import single_source_dijkstra

from core.model.change_and_go import CGType, CGNode, CGEdge
from core.model.graph import Graph
from core.model.lines import LinePool
from core.model.od import OD
from core.model.ptn import Link, Stop
from core.util.change_and_go_method import buildCGGraph
from core.util.statistic import Statistic
from helper import transform_cg_into_networkx
from multi_commodity_flow import capacitated_multi_commodity_flow
from parameters import Parameters

logger = logging.getLogger(__name__)


def change_and_go_evaluation(ptn: Graph[Stop, Link], lines: LinePool, od: OD, parameters: Parameters, statistic: Statistic):

    change_and_go = buildCGGraph(lines.getLineConcept(),
                                 parameters.ean_model_weight_drive,
                                 parameters.ean_model_weight_wait,
                                 parameters.ean_model_weight_change,
                                 parameters.ean_change_penalty,
                                 parameters.min_transfer_time,
                                 parameters.period_length,
                                 parameters.min_wait_time,
                                 parameters.max_wait_time,
                                 ptn.isDirected(),
                                 use_frequencies=True,
                                 create_transfers=True)
    cgn_path_difference = 2 * parameters.ean_change_penalty + 2 * parameters.period_length - parameters.wait_time
    shortest_paths_in_cg(change_and_go, ptn, od, statistic, cgn_path_difference)
    logger.debug("Compute capacitated travel time")
    cap_travel_time, cap_transfers = capacitated_multi_commodity_flow(change_and_go, ptn, od, lines, parameters,
                                                                      cgn_path_difference)
    statistic.setValue("lc_capacitated_perceived_time_average", cap_travel_time)
    statistic.setValue("lc_capacitated_prop_changes", cap_transfers)
    logger.debug("Compute travel time model travel time")
    # Now build to model travel time model behavior
    change_and_go = buildCGGraph(lines.getLineConcept(),
                                 parameters.ean_model_weight_drive,
                                 parameters.ean_model_weight_wait,
                                 parameters.ean_model_weight_change,
                                 parameters.ean_change_penalty,
                                 parameters.min_transfer_time,
                                 parameters.period_length,
                                 parameters.min_wait_time,
                                 parameters.max_wait_time,
                                 ptn.isDirected(),
                                 use_frequencies=False,
                                 create_transfers=True)
    cgn_path_difference = 2 * parameters.ean_change_penalty + 2 * parameters.min_transfer_time - parameters.wait_time
    cap_travel_time, cap_transfers = capacitated_multi_commodity_flow(change_and_go, ptn, od, lines, parameters,
                                                                      cgn_path_difference)
    statistic.setValue("lc_obj_travel_time", cap_travel_time)


def shortest_paths_in_cg(cgn: Graph[CGNode, CGEdge], ptn: Graph[Stop, Link], od: OD, statistic: Statistic,
                         cgn_path_difference: float):
    cgn_nx = transform_cg_into_networkx(cgn)

    total_travel_time_cg = 0
    number_of_changes = 0
    number_of_passengers = 0

    od_nodes = {}
    for cgnode in cgn.getNodes():
        if cgnode.getLineId() == 0:
            od_nodes[cgnode.getStopId()] = cgnode

    logger.debug("start computing shortest paths in (reduced) Networkx Change & Go Network")

    sp_cgn = {
        cgn_node: single_source_dijkstra(G=cgn_nx, source=cgn_node, weight="weight")
        for cgn_node in od_nodes.values()
    }

    logger.debug("iterate od-pairs")
    for ptn_origin in ptn.getNodes():
        for ptn_destination in ptn.getNodes():
            passengers = od.getValue(ptn_origin.getId(), ptn_destination.getId())
            number_of_passengers += passengers
            if ptn_origin == ptn_destination or passengers == 0:
                continue
            od_origin = od_nodes[ptn_origin.getId()]
            od_destination = od_nodes[ptn_destination.getId()]
            l_cgn = sp_cgn[od_origin][0].get(od_destination, float("inf"))
            # We counted both boarding and alighting as an od edge with length change_penalty, need to substract this
            l_cgn -= cgn_path_difference
            path_cgn = sp_cgn[od_origin][1][od_destination]
            path = nx.path_graph(path_cgn)
            for edge in path.edges():
                if cgn_nx.edges[edge[0], edge[1]]['object'].getType() == CGType.TRANSFER:
                    number_of_changes += passengers
            total_travel_time_cg += passengers * l_cgn

    logger.info("shortest paths in reduced Networkx Change & Go Network done")
    statistic.setValue("lc_perceived_time_average",
                       total_travel_time_cg / number_of_passengers)
    statistic.setValue("lc_prop_changes", number_of_changes)