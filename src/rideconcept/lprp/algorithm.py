import logging

from core.model.graph import Graph
from core.model.lines import LinePool
from core.model.ridepool import RidePool
from core.model.ptn import Stop, Link
from core.solver.generic_solver_interface import Solver, VariableType, ConstraintSense, OptimizationSense, Status, \
    IntAttribute
from core.solver.solver_parameters import SolverParameters

logger = logging.getLogger(__name__)


def solve_rideconcept_alpha(ptn: Graph[Stop, Link], line_pool: LinePool, ride_pool: RidePool, parameters: SolverParameters,
                      capacity_line: int, capacity_ridepool: int, objective: bool = True) -> bool:
    solver = Solver.createSolver(parameters.getSolverType())
    model = solver.createModel()

    logger.debug("Add variables")
    frequencies = {}
    for line in line_pool.getLines():
        if objective:
            var_frequency = model.addVariable(0, float('inf'), VariableType.INTEGER, line.getCost(), f"f_{line.getId()}")
        else:
            var_frequency = model.addVariable(0, float('inf'), VariableType.INTEGER, 0, f"f_{line.getId()}")
        frequencies[line] = var_frequency

    number_vehicles = {}
    for area in ride_pool.getAreas():
        if objective:
            var_number_vehicles = model.addVariable(0, float('inf'), VariableType.INTEGER, ride_pool.getCost(), f"v_{area.getId()}")
        else:
            var_number_vehicles = model.addVariable(0, float('inf'), VariableType.INTEGER, 0, f"v_{area.getId()}")
        number_vehicles[area] = var_number_vehicles

    logger.debug("Adding constraints")
    for link in ptn.getEdges():
        sum_per_link = model.createExpression()
        for line in line_pool.getLines():
            if link in line.getLinePath().getEdges():
                sum_per_link.multiAdd(capacity_line, frequencies[line])

        for area in ride_pool.getAreas():
            if link in area.getEdges():
                sum_per_link.multiAdd(capacity_ridepool*area.getDemandFactor(link), number_vehicles[area])
        model.addConstraint(sum_per_link, ConstraintSense.LESS_EQUAL, capacity_line*link.getUpperFrequencyBound(),
                            f"u_{link.getId()}")
        model.addConstraint(sum_per_link, ConstraintSense.GREATER_EQUAL, link.getLoad(),
                            f"l_{link.getId()}")
        

    logger.debug("Add parameters")
    parameters.setSolverParameters(model)
    model.setSense(OptimizationSense.MINIMIZE)
    if parameters.writeLpFile():
        logger.debug("Writing lp file")
        model.write("rideconcept.lp")
        logger.debug("Finished writing lp file")

    logger.debug("Start optimization")
    model.solve()
    logger.debug("Finished optimization")

    status = model.getStatus()
    if model.getIntAttribute(IntAttribute.NUM_SOLUTIONS) > 0:
        if status == Status.OPTIMAL:
            logger.debug("Optimal solution found")
        else:
            logger.debug("Feasible solution found")
        for line in line_pool.getLines():
            line.setFrequency(int(round(model.getValue(frequencies[line]))))
        for area in ride_pool.getAreas():
            area.setNumberOfVehicles(int(round(model.getValue(number_vehicles[area]))))
        model.dispose()
        solver.dispose()
        return True
    else:
        logger.debug("No feasible solution found")
        if status == Status.INFEASIBLE:
            model.computeIIS("rideconcept.ilp")
        model.dispose()
        solver.dispose()
        return False



def solve_rideconcept_beta(ptn: Graph[Stop, Link], line_pool: LinePool, ride_pool: RidePool, parameters: SolverParameters,
                      capacity_line: int, capacity_ridepool: int, period: int, objective: bool = True) -> bool:
    solver = Solver.createSolver(parameters.getSolverType())
    model = solver.createModel()

    logger.debug("Add variables")
    frequencies = {}
    for line in line_pool.getLines():
        if objective:
            var_frequency = model.addVariable(0, float('inf'), VariableType.INTEGER, line.getCost(), f"f_{line.getId()}")
        else:
            var_frequency = model.addVariable(0, float('inf'), VariableType.INTEGER, 0, f"f_{line.getId()}")
        frequencies[line] = var_frequency

    number_vehicles = {}
    for area in ride_pool.getAreas():
        if objective:
            var_number_vehicles = model.addVariable(0, float('inf'), VariableType.INTEGER, ride_pool.getCost(), f"v_{area.getId()}")
        else:
            var_number_vehicles = model.addVariable(0, float('inf'), VariableType.INTEGER, 0, f"v_{area.getId()}")
        number_vehicles[area] = var_number_vehicles

    beta = {}
    for area in ride_pool.getAreas():
        beta[area] = {}
        for edge in area.getEdges():
            var_beta = model.addVariable(0, float('inf'), VariableType.CONTINOUS, 0, f"beta_{area.getId()}_{edge.getId()}")
            beta[area][edge] = var_beta


    logger.debug("Adding constraints")
    for link in ptn.getEdges():
        sum_per_link = model.createExpression()
        for line in line_pool.getLines():
            if link in line.getLinePath().getEdges():
                sum_per_link.multiAdd(capacity_line, frequencies[line])

        for area in ride_pool.getAreas():
            if link in area.getEdges():
                sum_per_link.multiAdd(capacity_ridepool, beta[area][link])
        model.addConstraint(sum_per_link, ConstraintSense.LESS_EQUAL, capacity_line*link.getUpperFrequencyBound(),
                            f"u_{link.getId()}")
        model.addConstraint(sum_per_link, ConstraintSense.GREATER_EQUAL, link.getLoad(),
                            f"l_{link.getId()}")
    
    for area in ride_pool.getAreas():
        sum_per_area = model.createExpression()
        for edge in area.getEdges():
            sum_per_area.multiAdd(edge.getLowerBound(), beta[area][edge])
        model.addConstraint(sum_per_area, ConstraintSense.LESS_EQUAL, period*number_vehicles[area], f"sum_beta_{area.getId()}")
        

    logger.debug("Add parameters")
    parameters.setSolverParameters(model)
    model.setSense(OptimizationSense.MINIMIZE)
    if parameters.writeLpFile():
        logger.debug("Writing lp file")
        model.write("rideconcept.lp")
        logger.debug("Finished writing lp file")

    logger.debug("Start optimization")
    model.solve()
    logger.debug("Finished optimization")

    status = model.getStatus()
    if model.getIntAttribute(IntAttribute.NUM_SOLUTIONS) > 0:
        if status == Status.OPTIMAL:
            logger.debug("Optimal solution found")
        else:
            logger.debug("Feasible solution found")
        for line in line_pool.getLines():
            line.setFrequency(int(round(model.getValue(frequencies[line]))))
        for area in ride_pool.getAreas():
            area.setNumberOfVehicles(int(round(model.getValue(number_vehicles[area]))))
            for edge in area.getEdges():
                area.setDemandFactor(edge, model.getValue(beta[area][edge]))
        model.dispose()
        solver.dispose()
        return True
    else:
        logger.debug("No feasible solution found")
        if status == Status.INFEASIBLE:
            model.computeIIS("rideconcept.ilp")
        model.dispose()
        solver.dispose()
        return False