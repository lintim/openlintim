import logging
import sys

from core.exceptions.config_exceptions import ConfigNoFileNameGivenException
from core.exceptions.algorithm_dijkstra import AlgorithmStoppingCriterionException
from core.io.config import ConfigReader
from core.io.ptn import PTNReader
from core.io.ridepool import RidepoolWriter, RidepoolReader
from core.io.lines import LineReader, LineWriter
from core.solver.solver_parameters import SolverParameters

from algorithm import solve_rideconcept_alpha, solve_rideconcept_beta

logger = logging.getLogger(__name__)

if __name__ == '__main__':
    logger.info("Begin reading configuration")
    if len(sys.argv) < 2:
        raise ConfigNoFileNameGivenException()
    config = ConfigReader.read(sys.argv[1])
    parameters = SolverParameters(config, "rc_")
    capacity_line = config.getIntegerValue("gen_passengers_per_vehicle")
    capacity_ridepool = config.getIntegerValue("rc_passengers_per_vehicle")
    rc_model = config.getStringValue("rc_model")
    period = config.getIntegerValue("period_length")
    logger.info("Finished reading configuration")

    logger.info("Begin reading input data")
    ptn = PTNReader.read(read_loads=True)
    rpool = RidepoolReader.read(ptn, read_number_vehicles=False)
    lpool = LineReader.read(ptn, read_frequencies=False)
    logger.info("Finished reading input data")

    logger.info("Begin computing ride-concept")

    if rc_model.lower().endswith("alpha"):
        feasible = solve_rideconcept_alpha(ptn, lpool, rpool, parameters, capacity_line, capacity_ridepool)
    elif rc_model.lower().endswith("beta"):
        feasible = solve_rideconcept_beta(ptn, lpool, rpool, parameters, capacity_line, capacity_ridepool, period)

    if not feasible:
        raise AlgorithmStoppingCriterionException("ride concept")

    logger.info("Finished computing ride-concept")

    logger.info("Begin writing output data")
    RidepoolWriter.write(rpool, write_ride_concept=True)
    LineWriter.write(lpool)
    logger.info("Finished writing output data")