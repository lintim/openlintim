import logging
from typing import Tuple, List
import time

import math
import gurobipy as gp
from gurobipy import GRB

from core.model.graph import Graph
from core.model.ptn import Link, Stop
from core.model.tariff import PriceMatrix
from core.model.tariff_types import TariffObjectiveType
from core.model.routing import Routing
from core.exceptions.solver_exceptions import SolverFoundNoFeasibleSolutionException

from price_matrix_helper import compute_price_matrix_network_distance, compute_price_matrix_beeline_distance

logger = logging.getLogger(__name__)


def compute_optimal_beeline_distance_tariff(reference_prices: PriceMatrix, ptn: Graph[Stop, Link], weights: List[float], taf_opt_type: TariffObjectiveType,
                                    threads: int, mip_gap: float, timelimit: int, write_lp: bool) -> Tuple[float, float, PriceMatrix]:
    """
    Computes a fixed price and a price per kilometer for a beeline distance tariff model. Depending on the selected TariffObjectiveType
    either the sum or the maximum of the absolute deviations from the reference prices is minimized.
    :param reference_prices: PriceMatrix with the reference prices
    :param ptn: the ptn
    :param weights: List of weights for objective function
    :param taf_opt_type: TariffObjectiveType, either sum oder maxmimum of absolute deviations
    :param threads: number of threads to use by the solver
    :param mip_gap: relative MIP optimality gap
    :param timelimit: timelimit for the solver
    :param write_lp: whether or not to write an lp file
    :return: fixed costs, costs per kilometer and price matrix
    """
    # Compute input lists
    reference_price_list = []
    distance_list = []
    for origin in ptn.getNodes():
        for destination in ptn.getNodes():
            if origin.getId() != destination.getId():
                reference_price_list.append(reference_prices.getValue(origin.getId(), destination.getId()))
                distance_list.append(math.dist([origin.getXCoordinate(), origin.getYCoordinate()],
                                            [destination.getXCoordinate(), destination.getYCoordinate()]))

    # Use the chosen optimization model for computing the beeline tariff
    if taf_opt_type == TariffObjectiveType.SUM_ABSOLUTE_DEVIATION:
        [beeline_fix, beeline_factor] = optimize_sum_abs_model(reference_price_list, distance_list, weights, threads, mip_gap, timelimit, write_lp)
    elif taf_opt_type == TariffObjectiveType.MAX_ABSOLUTE_DEVIATION:
        [beeline_fix, beeline_factor] = optimize_max_abs_model(reference_price_list, distance_list, weights, threads, mip_gap, timelimit, write_lp)

    logger.info(f"fix costs beeline distance: {beeline_fix:.2f}")
    logger.info(f"factor costs beeline distance: {beeline_factor:.2f}")

    # Compute price matrix
    price_matrix = compute_price_matrix_beeline_distance(ptn, beeline_fix, beeline_factor)

    return beeline_fix, beeline_factor, price_matrix


def compute_optimal_network_distance_tariff(reference_prices: PriceMatrix, ptn: Graph[Stop, Link], routing: Routing, weights: List[float], taf_opt_type: TariffObjectiveType,
                                    threads: int, mip_gap: float, timelimit: int, write_lp: bool) -> Tuple[float, float, PriceMatrix]:
    """
    Computes a fixed price and a price per kilometer for a network distance tariff model. Depending on the selected TariffObjectiveType
    either the sum or the maximum of the absolute deviations from the reference prices is minimized.
    :param reference_prices: PriceMatrix with the reference prices
    :param ptn: the ptn
    :param weights: list of weights in objcetive function
    :param taf_opt_type: TariffObjectiveType, either sum oder maxmimum of absolute deviations
    :param threads: number of threads to use by the solver
    :param mip_gap: relative MIP optimality gap
    :param timelimit: timelimit for the solver
    :param write_lp: whether or not to write an lp file
    :return: fixed costs, costs per kilometer and price matrix
    """
    # compute lengths of paths w.r.t. distance
    paths_lengths = routing.getPathLengths()

    # Compute input lists
    reference_price_list = []
    distance_list = []
    for origin in ptn.getNodes():
        for destination in ptn.getNodes():
            if origin.getId() != destination.getId():
                reference_price_list.append(reference_prices.getValue(origin.getId(), destination.getId()))
                distance_list.append(paths_lengths[origin][destination])

    # Use the chosen optimization model for computing the distance tariff
    if taf_opt_type == TariffObjectiveType.SUM_ABSOLUTE_DEVIATION:
        [distance_fix, distance_factor] = optimize_sum_abs_model(reference_price_list, distance_list, weights, threads, mip_gap, timelimit, write_lp)
    elif taf_opt_type == TariffObjectiveType.MAX_ABSOLUTE_DEVIATION:
        [distance_fix, distance_factor] = optimize_max_abs_model(reference_price_list, distance_list, weights, threads, mip_gap, timelimit, write_lp)

    logger.info(f"fix costs network distance: {distance_fix:.2f}")
    logger.info(f"factor costs network distance: {distance_factor:.2f}")

    # Compute Price matrix
    price_matrix = compute_price_matrix_network_distance(ptn, paths_lengths, distance_fix, distance_factor)

    return distance_fix, distance_factor, price_matrix


def optimize_sum_abs_model(ref: List[float], dist: List[float], weights: List[float],
                           threads: int, mip_gap: float, timelimit: int, write_lp: bool) -> Tuple[float, float]:
    """
    Compute a fixed price and a price per kilometer minimizing the sum of absolute deviations from reference prices.
    :param ref: list of reference prices per OD-pair
    :param dist: list of lengths of path per OD-pair
    :param weights: list of weights in the objective function
    :param threads: number of threads to use by the solver
    :param mip_gap: relative MIP optimality gap
    :param timelimit: timelimit for the solver
    :param write_lp: whether or not to write an lp file
    :return: base amount f, price per kilometer p (for a network distance or beeline distance tariff)
    """

    NUMBER_OF_OD_PAIRS = len(weights)


    with gp.Env(empty=True) as env:
        env.setParam('OutputFlag', 0)
        env.start()
        with gp.Model(env=env) as m:

            if timelimit > -1:
                m.setParam("Timelimit", timelimit)
            if mip_gap > 0:
                m.setParam("MIPGap", mip_gap)
            if threads > -1:
                m.setParam("Threads", threads)

            var_fix_price = m.addVar(lb=0.0, ub=float('inf'), vtype=GRB.CONTINUOUS)
            var_factor_price = m.addVar(lb=0.0, ub=float('inf'), vtype=GRB.CONTINUOUS)
            var_diff_prices_new_ref = m.addVars(range(NUMBER_OF_OD_PAIRS))

            m.addConstrs(ref[d] - var_fix_price - var_factor_price * dist[d] <= var_diff_prices_new_ref[d] for d in range(NUMBER_OF_OD_PAIRS))
            m.addConstrs(-ref[d] + var_fix_price + var_factor_price * dist[d] <= var_diff_prices_new_ref[d] for d in range(NUMBER_OF_OD_PAIRS))
            m.setObjective(gp.quicksum(weights[d] * var_diff_prices_new_ref[d] for d in range(NUMBER_OF_OD_PAIRS)), GRB.MINIMIZE)

            if write_lp:
                m.write("tariff/tariff_dist_based_sum.lp")

            logger.debug("Start optimization")
            start_time = time.time()
            m.optimize()
            end_time = time.time()
            logger.info("Solver time: {:5.3f}s".format(end_time - start_time))

            if m.Status == GRB.INFEASIBLE:
                logger.error("Model is infeasible, compute IIS")
                m.computeIIS()
                m.write('tariff/iismodel.ilp')
                logger.error("Finished computing IIS, written to iismodel.ilp")
                raise SolverFoundNoFeasibleSolutionException()

            if m.SolCount == 0:
                logger.error("No feasible solution was found!")
                raise SolverFoundNoFeasibleSolutionException()
            else:
                logger.info("Feasible solution was found!")

            return var_fix_price.X, var_factor_price.X


def optimize_max_abs_model(ref: List[float], dist: List[float], weights: List[float],
                           threads: int, mip_gap: float, timelimit: int, write_lp: bool) -> Tuple[float, float]:
    """
    Compute a fixed price and a price per kilometer minimizing the maximum of absolute deviations from reference prices.
    :param ref: list of reference prices per OD-pair
    :param dist: list of lengths of path per OD-pair
    :param weights: list of weights in the objective function
    :param threads: number of threads to use by the solver
    :param mip_gap: relative MIP optimality gap
    :param timelimit: timelimit for the solver
    :param write_lp: whether or not to write an lp file
    :return: base amount f, price per kilometer p (for a network distance or beeline distance tariff)
    """

    NUMBER_OF_OD_PAIRS = len(weights)

    with gp.Env(empty=True) as env:
        env.setParam('OutputFlag', 0)
        env.start()
        with gp.Model(env=env) as m:

            if timelimit > -1:
                m.setParam("Timelimit", timelimit)
            if mip_gap > 0:
                m.setParam("MIPGap", mip_gap)
            if threads > -1:
                m.setParam("Threads", threads)

            var_fix_price = m.addVar(lb=0.0, ub=float('inf'), vtype=GRB.CONTINUOUS)
            var_factor_price = m.addVar(lb=0.0, ub=float('inf'), vtype=GRB.CONTINUOUS)
            var_diff_prices_new_ref = m.addVar(lb=0.0, ub=float('inf'), vtype=GRB.CONTINUOUS)

            m.addConstrs(weights[d] * ref[d] - weights[d] * var_fix_price - weights[d] * var_factor_price * dist[d] <= var_diff_prices_new_ref for d in range(NUMBER_OF_OD_PAIRS))
            m.addConstrs(weights[d] * -ref[d] + weights[d] * var_fix_price + weights[d] * var_factor_price * dist[d] <= var_diff_prices_new_ref for d in range(NUMBER_OF_OD_PAIRS))

            m.setObjective(var_diff_prices_new_ref, GRB.MINIMIZE)

            if write_lp:
                m.write("tariff/tariff_dist_based_max.lp")

            logger.debug("Start optimization")
            start_time = time.time()
            m.optimize()
            end_time = time.time()
            logger.info("Solver time: {:5.3f}s".format(end_time - start_time))

            if m.Status == GRB.INFEASIBLE:
                logger.error("Model is infeasible, compute IIS")
                m.computeIIS()
                m.write('tariff/iismodel.ilp')
                logger.error("Finished computing IIS, written to iismodel.ilp")
                raise SolverFoundNoFeasibleSolutionException()

            if m.SolCount == 0:
                logger.error("No feasible solution was found!")
                raise SolverFoundNoFeasibleSolutionException()
            else:
                logger.info("Feasible solution was found!")

            return var_fix_price.X, var_factor_price.X

