import gurobipy as gp
from gurobipy import GRB
import time
import logging

from core.exceptions.solver_exceptions import SolverFoundNoFeasibleSolutionException

logger = logging.getLogger(__name__)

def compute_flat_max_abs_weighted(reference_prices, weights, threads, mip_gap, timelimit, write_lp) -> float:
    with gp.Env(empty=True) as env:
        env.setParam('OutputFlag', 0)
        env.start()
        with gp.Model(env=env) as m:

            if timelimit > -1:
                m.setParam("Timelimit", timelimit)
            if mip_gap > 0:
                m.setParam("MIPGap", mip_gap)
            if threads > -1:
                m.setParam("Threads", threads)

            var_objective = m.addVar(lb=0.0, ub=float('inf'), vtype=GRB.CONTINUOUS)
            var_fix_price = m.addVar(lb=0.0, ub=float('inf'), vtype=GRB.CONTINUOUS)
            var_price_diff = m.addVar(lb=0.0, ub=float('inf'), vtype=GRB.CONTINUOUS)

            m.addConstrs(reference_price - var_fix_price <= var_price_diff for reference_price in reference_prices)
            m.addConstrs(var_fix_price - reference_price <= var_price_diff for reference_price in reference_prices)
            m.addConstrs(var_objective >= weight * var_price_diff for weight in weights)

            m.setObjective(var_objective, GRB.MINIMIZE)

            if write_lp:
                m.write("tariff/tariff_flat_max_weighted.lp")

            start_time = time.time()
            m.optimize()
            end_time = time.time()
            logger.info("Solver time: {:5.3f}s".format(end_time - start_time))

            if m.Status == GRB.INFEASIBLE:
                logger.error("Model is infeasible, compute IIS")
                m.computeIIS()
                m.write('tariff/iismodel.ilp')
                logger.error("Finished computing IIS, written to iismodel.ilp")
                raise SolverFoundNoFeasibleSolutionException()

            if m.SolCount == 0:
                logger.error("No feasible solution was found!")
                raise SolverFoundNoFeasibleSolutionException()
            else:
                logger.info("Feasible solution was found!")

            price = round(var_fix_price.X, 2)
            return price