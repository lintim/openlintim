# Jgrapht

## Version

1.5.0

## Filename

jgrapht-core-1.5.0.jar
jheaps-0.13.jar

## Project Homepage

https://jgrapht.org/

## Needed by

- ptn load generator (src/tools/ptn-load-generator)
- shortest path essentials library (src/essentials/shortest-paths)
- complete line pool (src/linepool/complete)
- line planning helper (src/essentials/lp-helper)
- robustness (sec/toolsrobustness_ml/java)
- line planning traveling time column generation approach (src/line-planning/traveling-time/column-generation-approach)
- line planning change minimization (src/line-planning/min-changes)
