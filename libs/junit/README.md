# JUnit

## Version

4.13.2

## Filename

junit-4.13.2.jar

## Project Homepage

https://junit.org/

## Needed by

- Java Core Unit Tests (src/core/java)
- Capacitated Timetable Evaluation Unit Tests (src/timetabling/periodic/capacitated_evaluation/test)